# README #

To get everything up and running  
1 go to root folder and run `npm install`  
2 go to client folder and run `npm install` there too  
3 while still in the client folder, run `npm run webpack` (-- --watch if you want it rebuilding if you make a code change)  
4 To start the web server, go back to root folder and run `npm start`  

### What is this repository for? ###

* The application answers the life long question of whether your choice of commuting is late and/or cancelled, so you won't have to leave the warmth of your home to stand by a bus/train stop needlessly.

### Who do I talk to? ###

* jurgenaut@gmail.com