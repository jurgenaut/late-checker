import React from 'react';
import StationSearch from 'station-searcher';
import JourneyControllerView from 'journey';

/**
 * Renders the two search components and a journeycontroller.
 */
export default class ContentPanelView extends React.Component {

	constructor(reactComponent) {
		super(reactComponent);
	}

	render() {
		
		return (<div style={styles.container}>
		    <div style={styles.flex}>
    			<StationSearch title={'Search from'} from={true} />
    			<StationSearch title={'Search to'} from={false} />
		    </div>
		    <div>
		        <JourneyControllerView /> 
		    </div>
		</div>);
		
	}
}

const styles = {
        container: {
            width: 'calc(100% - 10px)',
            borderColor: 'rgb(153, 222, 255)',
            borderWidth: '0 5px',
            borderStyle: 'solid'
        },
        flex: {
            display: 'flex',
            width: '100%',
            flexDirection: 'horizontal',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            paddingBottom: '10px'
        }   
    };
