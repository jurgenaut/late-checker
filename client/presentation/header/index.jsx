import React from 'react';


/**
 * Just a static header.
 */
export default class HeaderView extends React.Component {
	render() {
		return <div style={styles.container}>{'Is your train late?'}</div>
	}
};

const styles = {
        container: {
            width: '100%',
            backgroundColor: 'rgb(153, 222, 255)',
            textAlign: 'center',
            fontFamily: 'Arial',
            fontSize: '32px',
            borderTopLeftRadius: '10px',
            borderTopRightRadius: '10px'
                
        }
    };