import React from 'react';

/**
 * Just a static footer.
 */
export default class FooterView extends React.Component {
	render() {
		return <div style={styles.container}></div>
	}
};

const styles = {
        container: {
            width: '100%',
            backgroundColor: 'rgb(153, 222, 255)',
            textAlign: 'center',
            borderBottomLeftRadius: '10px',
            borderBottomRightRadius: '10px',
            height: '30px'    
        }
    };
