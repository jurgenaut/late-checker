import React from 'react';
import Services from 'services.js';

const DATE_TIME_DIVIDER = 'T';

/**
 * Renders relevant information for the a journey. 
 */
export default class JourneyView extends React.Component {
    
    constructor(reactComponent) {
        super(reactComponent);
    }
    
    render() {
        const journey = this.props.journey;
        if (!journey) {
            return null;
        }
        const realTime = journey.RouteLinks[0].RouteLink[0].RealTime;
        const realTimeIsObject = typeof realTime === 'object';
        
        const canceled = realTimeIsObject ? realTime.Canceled : false; 
        const late = realTimeIsObject ? realTime.DepTimeDeviation : 0;
        
        const canceledText = canceled ? <div>{'CANCELED'}</div> : null;
        const lateText = late > 0 ? <div>{`${late} minutes late!`}</div> : null;
        const onTime = (!canceledText && !lateText) ? <div>{'and it is on time!'}</div> : null;
        
        const nextJourney = journey.DepDateTime[0].split(DATE_TIME_DIVIDER)[1]; 
        
        return <div style={styles.container}>
            <div>{'Next journey is: '}</div>
            <div>{nextJourney}</div>
            {canceledText}
            {lateText}
            {onTime}
        </div>;
            
    }
}

const styles = {
        container: {
            textAlign: 'center',
            fontFamily: 'Arial',
            fontSize: '24px'
        }
};
