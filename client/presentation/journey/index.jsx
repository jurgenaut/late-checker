import React from 'react';
import Services from 'services.js';
import JourneyView from './view';

const JourneyStore = Services.Journey.store;

/**
 * Responsible for flowing data to the journey view child.
 */
export default class JourneyControllerView extends React.Component {
    constructor(reactComponent) {
        super(reactComponent);
        this.onJourneyStoreChanged = this.onJourneyStoreChanged.bind(this);
        
        this.state = {
           journey: null
        }
    }
    
    componentDidMount() {
        JourneyStore.listen(this.onJourneyStoreChanged);
    }
    
    componentWillUnmount() {
        JourneyStore.unlisten(this.onJourneyStoreChanged);
    }
    
    onJourneyStoreChanged() {
        if (JourneyStore.journeys()) {
            this.setState({
                journey: JourneyStore.journeys()[0]
            });    
        }
    }
    
    render() {
        return this.state.journey ? <JourneyView journey={this.state.journey} /> : null;
    }
}