import React from 'react';
import PropTypes from 'prop-types';

/**
 * This class is responsible for rendering the search results panel, and calling back to its parent when a station is clicked.
 */
export default class ResultsView extends React.Component {
    constructor(reactComponent) {
        super(reactComponent);
    }
    
    render() {
        var stations = [];
        if (this.props.results) {
            this.props.results.forEach(result => {
                const elem = <div key={result.Id} style={styles.stationRow} 
                    onClick={() => this.props.callback(result)}>{result.Name}</div>;
               stations.push(elem) 
            });
        }
        
        return this.props.results ? (<div style={{width: '100%'}}><div style={styles.resultsBox}>{stations}</div></div>) : null;
    }
    
}

const styles = {
        container: {
            width: '100%',
            textAlign: 'center'
        },
        resultsBox: {
            width: '200px',
            border: '1px black solid',
            backgroundColor: 'rgba(255,255,255,0.8)',
            maxHeight: '200px',
            overflowY: 'auto',
            display: 'inline-block'
        },
        stationRow: {
            cursor: 'pointer'
        }
    };

ResultsView.propTypes = {
    results: PropTypes.array,
    callback: PropTypes.func,
    visible: PropTypes.bool
}