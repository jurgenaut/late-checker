import React from 'react';
import PropTypes from 'prop-types';
import Services from 'services.js';
import ResultsView from './results';

const StationActions = Services.Station.actions;
const StationStore = Services.Station.store;

const MINIMUM_SEARCH_THRESHOLD = 2;


/** 
 *  This class renders a search box, either the from or to station.
 *  It also keeps track of data flow to its child ResultsView. 
 */
export default class StationSearchControllerView extends React.Component {
    
    constructor(reactComponent) {
        super(reactComponent);
        this.changeCallback = this.changeCallback.bind(this);
        this.resultsCallback = this.resultsCallback.bind(this);
        this.onStationStoreChanged = this.onStationStoreChanged.bind(this);
        this.textInput = null;
        
        this.state = {
            results: null,
            resultsVisible: false
        }
    }
    
    componentDidMount() {
        StationStore.listen(this.onStationStoreChanged);
    }
    
    componentWillUnmount() {
        StationStore.unlisten(this.onStationStoreChanged);
    }
    
    onStationStoreChanged() {
        this.setState({
            results: this.props.from ? StationStore.fromResults() : StationStore.toResults()            
        });
    }
    
    changeCallback() {
        if (this.textInput.value.length > MINIMUM_SEARCH_THRESHOLD) {
            StationActions.searchStations(this.textInput.value, this.props.from);
            this.setState({
                resultsVisible: true
            });
        } else {
            this.setState({
                resultsVisible: false
            });
        }
    }
    
    resultsCallback(station) {
        this.textInput.value = station.Name;
        StationActions.selectStation(station, this.props.from);
        this.setState({
            resultsVisible: false
        });
    }
    
	render() {
	    const results = this.state.resultsVisible ? <ResultsView results={this.state.results} callback={this.resultsCallback} /> : null;
	    
		return (<div style={styles.container}>
		    <div>
    		    <div style={styles.title}>{this.props.title}</div>
    		    <input type={'text'} ref={(input) => { this.textInput = input; }} onChange={this.changeCallback}></input>
    		</div>
    		{results}
		    
		</div>);
	}
};

const styles = {
        container: {
            flex: '1 1 50%',
            textAlign: 'center'
        },
        title: {
            fontFamily: 'Arial',
            fontSize: '13px'
        }
    };


StationSearchControllerView.propTypes = {
    title: PropTypes.string,
    from: PropTypes.bool
};