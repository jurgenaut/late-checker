import React from 'react';
import ReactDOM from 'react-dom';
import Header from 'header';
import Content from 'content';
import Footer from 'footer';

const styles = {
	container: {
		width: '50%'
	}
};

/**
 * This is the root in the component tree, it renders into the div with id "main".
 */

export default class RootView extends React.Component {
	contructor(component) {
		
	}
	
	render() {
		return (<div style={styles.container}>
			<Header />
			<Content />
			<Footer />
		</div>);
	}
}

document.addEventListener("DOMContentLoaded", (event) => {
	ReactDOM.render(<RootView/>, document.getElementById('main'));
});
