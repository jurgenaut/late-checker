const path = require('path');
const webpack = require('webpack');

const ROOT_DIR = `${__dirname}`;
const PRESENTATION_DIR = `${ROOT_DIR}/presentation`;
const SERVICE_DIR = `${ROOT_DIR}/service`;

module.exports = {
	entry : './index.jsx',
	output : {
		path : path.resolve(__dirname + '/../public/', 'build'),
		filename : 'app.bundle.js'
	},
	module : {
		loaders : [ {
			test : /\.js[x]*$/,
			loader : 'babel-loader',
			query : {
				presets : [ 'react', [ 'env', {
					'targets' : {
						'browsers' : '> 2%'
					}
				} ] ]
			}
		} ]
	},
	stats : {
		colors : true
	},
	devtool : 'source-map',
	resolve : {
		extensions : [ '.js', '.jsx' ],
		modules : [ PRESENTATION_DIR, SERVICE_DIR, 'node_modules' ]
	}
};