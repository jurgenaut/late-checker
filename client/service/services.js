import SearchActions from 'station/actions.js';
import SearchStore from 'station/store.js';
import SearchRetriever from 'station/retriever.js';

import JourneyActions from 'journey/actions.js';
import JourneyStore from 'journey/store.js';
import JourneyRetriever from 'journey/retriever.js';


class Services {
	constructor() {
		this.Station = {
			actions: SearchActions,
			store: SearchStore,
			retriever: SearchRetriever
		};
		this.Journey = {
			actions: JourneyActions,
			store: JourneyStore,
			retriever: JourneyRetriever
		};
	}
}

const instance = new Services();
export default instance;