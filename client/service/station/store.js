import alt from '../../alt';
import StationActions from './actions.js';
import DataRetriever from './retriever.js';

class StationStore {
	constructor() {
		this._fromResults = null;
		this._toResults = null;
		
		this._selectedStations = {
			from: null,
			to: null
		}
		
		this.bindListeners({
			handleSearchStation: StationActions.SEARCH_STATIONS,
			handleSearchSuccess: StationActions.SEARCH_SUCCESS,
			handleSelectStation: StationActions.SELECT_STATION
		});
		this.exportPublicMethods({
			fromResults: this.fromResults,
			toResults: this.toResults,
			selectedStations: this.selectedStations
        });
	}
	
	handleSearchStation(params) {
		DataRetriever.searchStations(params);
	}
	
	handleSearchSuccess(params) {
		if (params.from) {
			this._fromResults = params.results;
		} else {
			this._toResults = params.results;
		}
	}
	
	fromResults() {
		return this.state._fromResults;
	}
	
	toResults() {
		return this.state._toResults;
	}
	
	handleSelectStation(params) {
		const from = params.from ? 'from' : 'to';
		this._selectedStations[from] = params.station;
	}
	
	selectedStations() {
		return this.state._selectedStations;
	}
}

module.exports = alt.createStore(StationStore, 'StationStore');