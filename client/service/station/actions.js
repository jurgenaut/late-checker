import alt from '../../alt';

class SearchActions {
	
	/**
	 * station is the search phrase, from is a boolean indicating if search is for from or to
	 */
	searchStations(station, from) {
		return {
			station: station,
			from: from
		}
	}
	
	searchSuccess(results, from) {
		return {
			results: results,
			from: from
		}
	}
	
	selectStation(station, from) {
		return {
			station: station,
			from: from
		}
	}
}

module.exports = alt.createActions(SearchActions);