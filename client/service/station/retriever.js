import SearchActions from './actions.js';
import * as request from 'superagent';

const url = 'http://localhost:3000/api/';
const searchStation = `${url}station`;

class SearchRetriever {
	
	searchStations(params) {
		request.post(searchStation)
			.send(params)
			.end((err, response) => {
				if (response && response.ok) {
					SearchActions.searchSuccess(response.body, params.from);
				}
			});
	}
}

const instance = new SearchRetriever();
export default instance;