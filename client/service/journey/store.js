import alt from '../../alt';
import JourneyActions from './actions.js';
import DataRetriever from './retriever.js';
import StationActions from 'station/actions.js';
import StationStore from 'station/store.js';

class JourneyStore {
	constructor() {
		this._journeys = null;
		
		this.bindListeners({
			handleStationSelected: StationActions.SELECT_STATION,
			handleSearchSuccess: JourneyActions.SEARCH_SUCCESS
		});
		this.exportPublicMethods({
			journeys: this.journeys
        });
	}
	
	handleStationSelected(params) {
		const selectedStations = StationStore.selectedStations();
		if (selectedStations.from && selectedStations.to) {
			DataRetriever.searchJourneys({
				from: selectedStations.from,
				to: selectedStations.to
			});	
		}
	}
	
	handleSearchSuccess(results) {
		if (results) {
			this._journeys = results;
		}
	}
	
	journeys() {
		return this.state._journeys;
	}
}

module.exports = alt.createStore(JourneyStore, 'JourneyStore');