import alt from '../../alt';

class JourneyActions {
	
	searchSuccess(results) {
		return results;
	}
}

module.exports = alt.createActions(JourneyActions);