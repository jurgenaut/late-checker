import JourneyActions from './actions';
import * as request from 'superagent';
import JourneyUtils from './utils';

const url = 'http://localhost:3000/api/';
const searchStation = `${url}journey`;

class JourneyRetriever {
	
	searchJourneys(params) {
		const requestParams = {
			from: JourneyUtils.createRequestStation(params.from),
			to: JourneyUtils.createRequestStation(params.to)
		};
		
		request.post(searchStation)
			.send(requestParams)
			.end((err, response) => {
				if (response && response.ok) {
					JourneyActions.searchSuccess(response.body);
				}
			});
	}
}

const instance = new JourneyRetriever();
export default instance;