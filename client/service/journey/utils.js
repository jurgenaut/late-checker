
export default class JourneyUtils {
	
	static createRequestStation(station) {
		return {
			name: station.Name[0],
			id: station.Id[0],
			type: 0
		}; 
	}
}