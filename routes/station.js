const skanetrafiken = require("node-skanetrafiken");

exports.searchStation = (request, response) => {
	const searchPhrase = request.body.station;
	
	skanetrafiken.findStop({name: searchPhrase}, (results, err) => {
		response.send(results);
	});
};