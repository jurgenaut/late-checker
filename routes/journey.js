const skanetrafiken = require("node-skanetrafiken");

exports.searchJourney = (request, response) => {
	const fromStation = request.body.from;
	const toStation = request.body.to;
	skanetrafiken.getJourneys({action: "next", from: fromStation, to: toStation, limit: 3}, (results, err) => {
		response.send(results);
	});
};